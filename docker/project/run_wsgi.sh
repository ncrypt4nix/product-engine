#!/bin/bash

generate_static() {
    yes yes | python /srv/project/backend/manage.py collectstatic
    npm install --prefix /srv/static/
    sass --force --update --no-cache --style compressed /srv/static/sass:/srv/static/css
}

python /srv/project/backend/manage.py migrate --noinput &
python /srv/project/backend/manage.py compilemessages &
generate_static &
celery -A engine.celery --workdir=/srv/project/backend worker -l info --beat --scheduler django_celery_beat.schedulers:DatabaseScheduler &
gunicorn --pythonpath backend engine.wsgi --bind 0.0.0.0:8000 -w $(($(nproc) + 1))
