from django.conf.urls import include
from django.urls import path
from django.contrib.sites.models import Site
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title=Site.objects.get_current().name,
        default_version='v1',
        description='Public api documentation for the site',
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="site@tayle.ru"),
        license=openapi.License(name="GNU Affero General Public License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('v1/', include('api.v1.urls'), name='v1'),
    path(
        'doc/',
        schema_view.with_ui('swagger', cache_timeout=0),
        name='schema-swagger-ui'
    ),
]
