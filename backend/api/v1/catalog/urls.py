from rest_framework import routers
from api.v1.catalog.views import SectionViewSet, ProductViewSet


router = routers.SimpleRouter()
router.register(r'sections', SectionViewSet)
router.register(r'products', ProductViewSet)

urlpatterns = router.urls
