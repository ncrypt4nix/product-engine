from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from api.v1.catalog.serializers import (
    SectionSerializer,
    ProductSerializer,
    CharSerializer,
    OrderTableSerializer,
)
from catalog.models import Section, Product


class SectionViewSet(viewsets.ReadOnlyModelViewSet):
    """Catalog: Sections"""
    queryset = Section.on_site.all()
    serializer_class = SectionSerializer
    lookup_field = 'ident'


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    """Catalog: Products"""
    queryset = Product.on_site.all()
    serializer_class = ProductSerializer
    lookup_field = 'art'

    @action(methods=['get'], detail=True)
    def chars(self, request, art):
        """Get Product Chars"""
        product = self.get_object()
        serializer = CharSerializer(product.get_char_set(), many=True)
        return Response(serializer.data)

    @action(methods=['get'], detail=True)
    def order_table(self, request, art):
        """Get Order Table"""
        product = self.get_object()
        serializer = OrderTableSerializer(
            product.subproduct_set.all(),
            many=True
        )
        return Response(serializer.data)
