from rest_framework import serializers
from catalog.models import Section, Product


class SectionSerializer(serializers.ModelSerializer):
    """Serializer for sections to open rest api to partners"""
    children = serializers.SerializerMethodField()
    products = serializers.SerializerMethodField()

    class Meta:
        model = Section
        fields = ['ident', 'title', 'description', 'children', 'products']

    def get_children(self, section):
        return [i.ident for i in section.children.all()]

    def get_products(self, section):
        if not section.is_leaf_node:
            return []
        return [i.art for i in section.products.all()]


class ProductSerializer(serializers.ModelSerializer):
    """Serializer for products to open rest api to partners"""
    order_table = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = [
            'art',
            'title',
            'description',
            'specification',
            'order_table'
        ]

    def get_order_table(self, product):
        return [i.art for i in product.subproduct_set.all()]


class CharSerializer(serializers.ModelSerializer):
    """Serializer for chars to open rest api to partners"""
    name = serializers.CharField(source='char.name')

    class Meta:
        model = Product.chars.through
        fields = ['name', 'value']


class OrderTableSerializer(serializers.ModelSerializer):
    """Serializer to SubProduct with chars"""
    chars = serializers.SerializerMethodField()

    class Meta:
        model = Product.SubProduct
        fields = ['art', 'chars']

    def get_chars(self, product):
        return {i.char.name: i.value for i in product.defined_chars}
