import abc
from django.db import models
from django.db.models import F
from django.utils.translation import ugettext_lazy as _
from django.apps import apps
from engine.core.metaclasses import AbstractModelMeta


class ParentCharMixin(models.Model):
    # TODO! docstring
    class Meta:
        abstract = True

    char_template = models.ManyToManyField(
        to='catalog.Char',
        related_name='+',
        symmetrical=False,
        blank=True,
        verbose_name=_('Template Chars'),
        help_text=_('Set of characteristics to define it later'),
    )

    def get_filters(self, child_set):
        filter_through = child_set.model.chars.through
        return filter_through.objects.filter(
            **{
                f'{filter_through.object_name}__in': child_set,
                'char__is_filter': True
            }
        )


class ChildCharMixin(models.Model, metaclass=AbstractModelMeta):
    # TODO! docstring
    class Meta:
        abstract = True

    char_parent = property(abc.abstractmethod(lambda _: _))

    def get_char_set(self, **query_kwargs: dict):
        # TODO! docstring
        parent_qs = self.char_parent.prefetch_related('chars')
        chars = parent_qs.model.chars.through.objects.none()
        for parent in parent_qs:
            kwargs = {
                parent.chars.through.object_name: parent.pk,
                'char__in': parent.chars.all()
            }
            chars |= parent.chars.through.objects.filter(**kwargs).filter(
                **query_kwargs).annotate(order_index=F('char__order_index'))
        return self.chars.through.objects.filter(
            **{self.chars.through.object_name: self.pk}
        ).filter(**query_kwargs).annotate(
            order_index=F('char__order_index')
        ).union(chars).order_by('order_index')

    @property
    def _parent_char_template(self):
        # TODO! docstring
        chars = apps.get_model('catalog', 'Char').objects.none()
        for parent in self.char_parent:
            chars |= parent.char_template.all()
        return chars

    @property
    def undefined_chars(self):
        """get undefined parent chars"""
        return self._parent_char_template.difference(self.chars.all())

    @property
    def defined_chars(self):
        """get defined parent chars"""
        kwargs = {
            self.chars.through.object_name: self.pk,
            'char__in': self._parent_char_template
        }
        return self.chars.through.objects.filter(**kwargs)

    @property
    def template_char_row(self):
        # TODO! docstring
        defined = {i.char: i for i in self.defined_chars}
        return [
            defined.get(char, None)
            for char in self._parent_char_template
        ]
