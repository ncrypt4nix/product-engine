import abc
from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from sortedm2m.fields import SortedManyToManyField
from engine.core.interfaces import ContentInterface
from engine.core.mixins import OnSiteMixin
from catalog.metaclasses import (
    ProductMeta,
    CharFabricMeta,
    ProductCharInterfaceMeta
)
from catalog.managers import ProductManager


class CharInterface(models.Model, metaclass=CharFabricMeta):
    # TODO! docstring
    class Meta:
        abstract = True


class CatalogInterface(
    ContentInterface
):
    # TODO! docstring
    class Meta:
        abstract = True

    _help_text = _(
        'For a particular product are assembled from a section and a product'
    )
    related_name_list = [
        'key_features',
        'accessories',
        'related_products',
        'optional_components',
        'resources',
    ]
    catalog_parent = property(abc.abstractmethod(lambda _: _))

    key_features = models.ManyToManyField(
        to='catalog.KeyFeature',
        related_name='+',
        symmetrical=False,
        blank=True,
        verbose_name=_('Key Features'),
        help_text=_help_text,
    )
    accessories = SortedManyToManyField(
        to='catalog.Product',
        related_name='+',
        symmetrical=False,
        blank=True,
        verbose_name=_('Accessories'),
        help_text=_help_text,
    )
    related_products = SortedManyToManyField(
        to='catalog.Product',
        related_name='+',
        symmetrical=False,
        blank=True,
        verbose_name=_('Related Products'),
        help_text=_help_text,
    )
    optional_components = SortedManyToManyField(
        to='catalog.Product',
        related_name='+',
        symmetrical=False,
        blank=True,
        verbose_name=_('Optional Components'),
        help_text=_help_text,
    )
    resources = models.ManyToManyField(
        to='catalog.Resource',
        related_name='+',
        symmetrical=False,
        blank=True,
        verbose_name=_('Resource'),
        help_text=_help_text,
    )

    def __getattr__(self, name):
        if not name.startswith('get_'):
            return object.__getattribute__(self, name)
        real_name = name.replace('get_', '', 1)
        if real_name not in self.related_name_list:
            return object.__getattribute__(self, name)
        qs = getattr(self, real_name).all()
        for parent in self.catalog_parent:
            qs |= getattr(parent, real_name).all()
        return qs


class ProductInterface(OnSiteMixin, metaclass=ProductMeta):
    # TODO! docstring
    class Meta:
        abstract = True
        indexes = [
            models.Index(fields=['art']),
        ]

    art = models.SlugField(
        _('P/N'),
        max_length=32,
        unique=True,
        db_index=True,
        help_text=_('Part Number'),
    )
    on_site = ProductManager()
    real_product = property(abc.abstractmethod(lambda _: _))
    real_product_pk = property(abc.abstractmethod(lambda _: _))

    def save(self, *args, **kwargs):
        self.clean()
        super().save(*args, **kwargs)

    def clean(self):
        for model in self.__class__.inheritors:
            if (model != self.__class__ and
                    model.objects.filter(art=self.art).exists()):
                raise ValidationError(
                    _('The product duplicates an existing article')
                )

    def __str__(self):
        return self.art


class ProductCharInterface(
    CharInterface,
    ProductInterface,
    metaclass=ProductCharInterfaceMeta
):
    # TODO! docstring
    class Meta:
        abstract = True
