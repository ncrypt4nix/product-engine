from modeltranslation.translator import register, TranslationOptions
from catalog.models import (
    Section,
    Product,
    KeyFeature,
    BasicComponent,
    Char,
    ResourceType,
    Resource,
)


@register(Section)
class SectionTranslationOptions(TranslationOptions):
    fields = ('title', 'description')


@register(Product)
class ProductTranslationOptions(TranslationOptions):
    fields = (
        'title',
        'description',
        'specification',
    )


@register(KeyFeature)
class KeyFeatureTranslationOptions(TranslationOptions):
    fields = ('title', 'alt')


@register(BasicComponent)
class BasicComponentTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Char)
class CharTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(ResourceType)
class ResourceTypeTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Resource)
class ResourceTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Product.CarouselImage)
class CarouselImageTranslationOptions(TranslationOptions):
    fields = ('caption', 'alt',)
