from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from catalog.tests.base import DataInitMixin


class TestCatalogViews(DataInitMixin, TestCase):
    def test_catalog_index(self):
        response = self.client.get(reverse('catalog:catalog-index'))
        self.assertEqual(response.status_code, 200)

    def test_catalog_section(self):
        self.assertEqual(
            reverse(
                'catalog:catalog_section',
                kwargs={'path': self.middle_section.get_url_path()}
            ),
            self.middle_section.get_absolute_url()
        )
        response = self.client.get(self.middle_section.get_absolute_url())
        self.assertEqual(response.status_code, 200)

    def test_catalog_product(self):
        self.assertEqual(
            reverse(
                'catalog:catalog_product',
                kwargs={
                    'section_path': self.leaf_section.get_url_path(),
                    'art': self.first_product.art
                }
            ),
            self.first_product.get_absolute_url()
        )
        response = self.client.get(self.first_product.get_absolute_url())
        self.assertEqual(response.status_code, 200)

    def test_catalog_product_datasheet(self):
        datasheet_url = reverse(
            'catalog:catalog_product_datasheet',
            kwargs={
                'section_path': self.leaf_section.get_url_path(),
                'art': self.first_product.art
            }
        )
        response = self.client.get(datasheet_url)
        self.assertEqual(response.status_code, 200)


class TestCompareAndCartViewSets(DataInitMixin, TestCase):
    def test_compare_viewset(self):
        url = reverse('catalog:compare-list')
        client = APIClient()
        # create
        response = client.put(
            reverse(
                'catalog:compare-detail',
                kwargs={'art': self.first_product.art}
            )
        )
        self.assertEqual(response.status_code, 200)
        # try to create duplicate
        response = client.put(
            reverse(
                'catalog:compare-detail',
                kwargs={'art': self.first_product.art}
            )
        )
        self.assertEqual(response.status_code, 200)
        # test sub product
        response = client.put(
            reverse(
                'catalog:compare-detail',
                kwargs={'art': self.sub_product_to_first.art}
            )
        )
        self.assertEqual(response.status_code, 200)
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)
        # destroy
        response = client.delete(
            reverse(
                'catalog:compare-detail',
                kwargs={'art': self.first_product.art}
            )
        )
        self.assertEqual(response.status_code, 200)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        # clear
        response = client.post(reverse('catalog:compare-clear'))
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)

    def test_cart_viewset(self):
        url = reverse('catalog:cart-list')
        client = APIClient()
        # create
        response = client.put(
            reverse(
                'catalog:cart-detail',
                kwargs={'art': self.first_product.art}
            )
        )
        self.assertEqual(response.status_code, 200)
        # try to create duplicate
        response = client.put(
            reverse(
                'catalog:cart-detail',
                kwargs={'art': self.first_product.art}
            )
        )
        self.assertEqual(response.status_code, 200)
        # test sub product
        response = client.put(
            reverse(
                'catalog:cart-detail',
                kwargs={'art': self.sub_product_to_first.art}
            )
        )
        self.assertEqual(response.status_code, 200)
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 2)
        # destroy
        response = client.delete(
            reverse(
                'catalog:cart-detail',
                kwargs={'art': self.first_product.art}
            )
        )
        self.assertEqual(response.status_code, 200)

        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        # clear
        response = client.post(reverse('catalog:cart-clear'))
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)
