from django.core.exceptions import ValidationError
from django.test import TestCase
from catalog.tests.base import DataInitMixin
from catalog.models import Section, Product


class TestSectionModel(DataInitMixin, TestCase):
    def test_get_url_path(self):
        need_path = self.root_section.ident
        self.assertEqual(need_path, self.root_section.get_url_path())
        need_path += '/' + self.middle_section.ident
        self.assertEqual(need_path, self.middle_section.get_url_path())
        need_path += '/' + self.leaf_section.ident
        self.assertEqual(need_path, self.leaf_section.get_url_path())

    def test_reverse_path_or_404(self):
        self.assertEqual(
            Section.on_site.reverse_path_or_404(self.root_section.ident),
            self.root_section
        )
        path = '/'.join([self.root_section.ident, self.middle_section.ident])
        self.assertEqual(
            Section.on_site.reverse_path_or_404(path),
            self.middle_section
        )
        path += '/' + self.leaf_section.ident
        self.assertEqual(
            Section.on_site.reverse_path_or_404(path),
            self.leaf_section
        )


class TestProductModel(DataInitMixin, TestCase):
    def test_unique_art(self):
        with self.assertRaises(ValidationError):
            Product.SubProduct.objects.create(
                product=self.first_product,
                art=self.first_product.art
            )


class TestCharInterface(DataInitMixin, TestCase):
    def test_get_char_set(self):
        chars = [i.char for i in self.first_product.get_char_set()]
        self.assertIn(self.section_defined_char, chars)
        self.assertIn(self.section_char, chars)
        self.assertIn(self.section_filter, chars)
        self.assertNotIn(self.section_template_char, chars)
        self.assertNotIn(self.section_template_filter, chars)
        self.assertIn(self.product_char, chars)
        self.assertIn(self.product_filter, chars)
        self.assertNotIn(self.product_template_char, chars)
        self.assertNotIn(self.product_template_filter, chars)

    def test__parent_char_template(self):
        chars = self.first_product._parent_char_template
        self.assertNotIn(self.section_char, chars)
        self.assertNotIn(self.section_filter, chars)

        self.assertIn(self.section_template_char, chars)
        self.assertIn(self.section_template_filter, chars)
        self.assertIn(self.section_defined_char, chars)

    def test_undefined_chars(self):
        chars = self.first_product.undefined_chars
        self.assertNotIn(self.section_char, chars)
        self.assertNotIn(self.section_filter, chars)
        self.assertNotIn(self.section_defined_char, chars)

        self.assertIn(self.section_template_char, chars)
        self.assertIn(self.section_template_filter, chars)

    def test_defined_chars(self):
        chars = [i.char for i in self.first_product.defined_chars]
        self.assertIn(self.section_defined_char, chars)

        self.assertNotIn(self.section_char, chars)
        self.assertNotIn(self.section_filter, chars)
        self.assertNotIn(self.section_template_char, chars)
        self.assertNotIn(self.section_template_filter, chars)

    def test_template_char_row(self):
        chars = [i.char for i in self.first_product.template_char_row if i]
        self.assertIn(self.section_defined_char, chars)

        self.assertNotIn(self.section_char, chars)
        self.assertNotIn(self.section_filter, chars)
        self.assertNotIn(self.section_template_char, chars)
        self.assertNotIn(self.section_template_filter, chars)

    def test_get_filters(self):
        filters = self.leaf_section.get_filters(
            self.leaf_section.products.all()
        )
        for filter in filters:
            self.assertTrue(filter.char.is_filter)

        chars = [i.char for i in filters]
        self.assertNotIn(self.section_filter, chars)
        self.assertNotIn(self.section_template_filter, chars)
        self.assertNotIn(self.product_template_filter, chars)
        self.assertIn(self.product_filter, chars)
