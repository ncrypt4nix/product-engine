from django.contrib.sites.models import Site
from engine.core.tests.base import TestBaseMixin
from catalog.models import Section, Product, Char


class DataInitMixin(TestBaseMixin):
    def setUp(self):
        super().setUp()
        # common data to catalog
        # sections
        self.root_section = Section.on_site.create(
            ident='root-node',
            icon=self.get_temp_image().name,
        )
        self.root_section.sites.add(Site.objects.get_current())
        self.middle_section = Section.on_site.create(
            ident='middle-node',
            icon=self.get_temp_image().name,
            parent=self.root_section
        )
        self.middle_section.sites.add(Site.objects.get_current())
        self.leaf_section = Section.on_site.create(
            ident='last-node',
            icon=self.get_temp_image().name,
            parent=self.middle_section
        )
        self.leaf_section.sites.add(Site.objects.get_current())
        # products
        self.first_product = Product.on_site.create(
            art='first-art',
            icon=self.get_temp_image().name,
        )
        self.first_product.sections.add(self.leaf_section)
        self.first_product.sites.add(Site.objects.get_current())

        self.sub_product_to_first = Product.SubProduct.objects.create(
            art='first-sub-product',
            product=self.first_product
        )

        # chars to section
        self.section_char = Char.objects.create(
            name='section-char',
            is_filter=False
        )
        self.leaf_section.chars.through.objects.create(
            section=self.leaf_section,
            char=self.section_char,
            value='section-char_value'
        )

        self.section_filter = Char.objects.create(
            name='section-filter',
            is_filter=True
        )
        self.leaf_section.chars.through.objects.create(
            section=self.leaf_section,
            char=self.section_filter,
            value='section-filter_value'
        )

        self.section_template_char = Char.objects.create(
            name='section-template-char',
            is_filter=False
        )
        self.section_template_filter = Char.objects.create(
            name='section-template-filter',
            is_filter=True
        )

        self.section_defined_char = Char.objects.create(
            name='section-defined-char',
            is_filter=False
        )
        self.first_product.chars.through.objects.create(
            product=self.first_product,
            char=self.section_defined_char,
            value='product-defined-char_value'
        )

        self.leaf_section.char_template.add(
            self.section_template_char,
            self.section_template_filter,
            self.section_defined_char,
        )

        # chars to product
        self.product_char = Char.objects.create(
            name='product-char',
            is_filter=False
        )
        self.first_product.chars.through.objects.create(
            product=self.first_product,
            char=self.product_char,
            value='product-char_value'
        )

        self.product_filter = Char.objects.create(
            name='product-filter',
            is_filter=True
        )
        self.first_product.chars.through.objects.create(
            product=self.first_product,
            char=self.product_filter,
            value='product-filter_value'
        )

        self.product_template_char = Char.objects.create(
            name='product-template-char',
            is_filter=False
        )
        self.product_template_filter = Char.objects.create(
            name='product-template-filter',
            is_filter=True
        )
        self.first_product.char_template.add(
            self.product_template_char, self.product_template_filter
        )
