from django.db import models
from django.utils.translation import ugettext_lazy as _
from engine.core.metaclasses import AbstractModelMeta, DjangoMPTTMeta


class ProductMeta(AbstractModelMeta):
    # TODO! docstring
    inheritors = []

    def __new__(cls, name, bases, dct):
        # TODO! docstring
        _class = super().__new__(cls, name, bases, dct)
        if not _class._meta.abstract:
            cls.inheritors.append(_class)
        return _class


class CharFabricMeta(AbstractModelMeta):
    # TODO! docstring
    def __new__(cls, name, bases, dct):
        # TODO! docstring
        if getattr(dct['Meta'], 'abstract', False):
            return super().__new__(cls, name, bases, dct)
        _through_fields = (name.lower(), 'char')

        class Meta:
            unique_together = _through_fields

        _through = type(
            f'{name}ToChar',
            (models.Model,),
            {
                '__module__': 'catalog',
                'Meta': Meta,
                'char': models.ForeignKey(
                    'catalog.Char',
                    on_delete=models.CASCADE
                ),
                name.lower(): models.ForeignKey(
                    f'catalog.{name}',
                    on_delete=models.CASCADE
                ),
                'value': models.CharField(
                    _('Value'),
                    max_length=128,
                    help_text=_('Characteristic value')
                ),
                'object_name': name.lower(),
            }
        )
        dct['chars'] = models.ManyToManyField(
            to='catalog.Char',
            verbose_name=_('Chars'),
            related_name='+',
            symmetrical=False,
            through=_through,
            through_fields=_through_fields,
            help_text=_('Defined chars'),
        )
        return super().__new__(cls, name, bases, dct)


class ProductCharInterfaceMeta(ProductMeta, CharFabricMeta):
    # TODO! docstring
    pass


class SectionMeta(DjangoMPTTMeta, CharFabricMeta):
    # TODO! docstring
    pass
