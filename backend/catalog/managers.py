from django.http import Http404
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.sites.managers import CurrentSiteManager
from catalog.metaclasses import ProductMeta


class SectionManager(CurrentSiteManager):
    # TODO! docstring
    def reverse_path_or_404(self, path):
        # TODO! docstring
        path_list = path.split('/')
        section = None
        if path_list[-1] == '' and len(path_list) == 2:
            path_list.pop(len(path_list) - 1)
        for _section in super().get_queryset().filter(ident=path_list[-1]):
            if _section.is_root_node() and len(path_list) == 1:
                return _section
            for i, parent in enumerate(_section.get_ancestors()):
                if parent.ident != path_list[i]:
                    section = None
                    break
                section = _section
            if section:
                return section
        raise Http404


class ProductManager(CurrentSiteManager):
    # TODO! docstring
    ONLY_PARAM = ('pk', 'art')

    def get_product_from_art(self, art: str):
        for _class in ProductMeta.inheritors:
            try:
                return _class.on_site.get(art=art)
            except ObjectDoesNotExist:
                pass
        raise ObjectDoesNotExist

    def filter_product_from_art_list(self, art_list: list):
        qs = None
        qs_list = []
        for _class in ProductMeta.inheritors:
            if not qs:
                qs = _class.objects.filter(art__in=art_list).only(
                    *self.ONLY_PARAM)
                continue
            qs_list.append(
                _class.objects.filter(art__in=art_list).only(*self.ONLY_PARAM)
            )
        return qs.union(*qs_list)

    def all_products(self):
        qs = super().get_queryset().only(*self.ONLY_PARAM)
        qs_list = []
        for _class in ProductMeta.inheritors:
            if _class == qs.model:
                continue
            qs_list.append(_class.objects.only(*self.ONLY_PARAM))
        if qs_list:
            return qs.union(*qs_list)
        return qs
