from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from catalog.models import Product


class ProductArtExistsForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ('art',)

    def clean(self):
        cleaned_data = super().clean()
        try:
            self.product = Product.on_site.get_product_from_art(
                art=cleaned_data.get('art')
            )
        except ObjectDoesNotExist:
            raise forms.ValidationError(
                _('Product with this art does not exist')
            )
