from django.forms.models import BaseInlineFormSet
from django.forms.widgets import HiddenInput
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from modeltranslation.admin import TranslationAdmin, TranslationTabularInline
from nested_admin import (
    NestedModelAdmin,
    NestedStackedInline,
    NestedTabularInline,
)
from nested_admin.forms import SortableHiddenMixin
from sorl.thumbnail.admin import AdminImageMixin
from adminsortable2.admin import SortableAdminMixin
from mptt.admin import DraggableMPTTAdmin
from catalog.models import (
    Section,
    Product,
    KeyFeature,
    BasicComponent,
    Char,
    ResourceType,
    Resource,
)


##############################################################################
#  Formsets
##############################################################################

class ChildCharFormset(BaseInlineFormSet):
    # TODO! docstring
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not kwargs['instance'] or not kwargs['instance'].pk:
            return
        self.initial = [
            {'char': char}
            for char in kwargs['instance'].undefined_chars
        ]


##############################################################################
#  Inline Mixins
##############################################################################

class CharToInlineMixin:
    # TODO! docstring
    formset = ChildCharFormset
    classes = ['collapse']

    def get_extra(self, request, obj=None, **kwargs):
        if not obj or not obj.pk:
            return 0
        return obj.undefined_chars.count()

##############################################################################
#  Inlines
##############################################################################


class BasicComponentCountInline(NestedTabularInline):
    # TODO! docstring
    model = Product.basic_components.through
    classes = ['collapse']
    extra = 0


class SectionInline(NestedStackedInline):
    # TODO! docstring
    model = Section.products.through
    fields = ('section',)
    classes = ['collapse']
    extra = 0
    verbose_name = _('Section')
    verbose_name_plural = _('Sections')


class CharToSubProductInline(CharToInlineMixin, NestedTabularInline):
    # TODO! docstring
    model = Product.SubProduct.chars.through
    verbose_name = _('Characteristic')
    verbose_name_plural = _('Sub Product Characteristics')


class CharToProductInline(CharToInlineMixin, NestedTabularInline):
    # TODO! docstring
    model = Product.chars.through
    verbose_name = _('Characteristic')
    verbose_name_plural = _('Product Characteristics')


class CharToSectionInline(NestedTabularInline):
    # TODO! docstring
    model = Section.chars.through
    classes = ['collapse']
    extra = 0
    verbose_name = _('Characteristic')
    verbose_name_plural = _('Characteristics in Section')


class CarouselImageInline(NestedTabularInline, TranslationTabularInline):
    # TODO! docstring
    model = Product.CarouselImage
    classes = ['collapse']
    fields = ('order_index', 'cfile_thumb', 'cfile', 'alt', 'caption')
    sortable_field_name = 'order_index'
    readonly_fields = ('cfile_thumb',)
    extra = 0

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        if db_field.name == self.sortable_field_name:
            kwargs["widget"] = HiddenInput()
        field = super().formfield_for_dbfield(db_field, request, **kwargs)
        self.patch_translation_field(db_field, field, request, **kwargs)
        return field


class SubProductInline(SortableHiddenMixin, NestedStackedInline):
    # TODO! docstring
    model = Product.SubProduct
    classes = ['collapse']
    sortable_field_name = 'order_index'
    fields = ('order_index', 'art')
    inlines = (CharToSubProductInline,)
    extra = 0


##############################################################################
#  Model Admins
##############################################################################

@admin.register(Section)
class SectionAdmin(DraggableMPTTAdmin, NestedModelAdmin, TranslationAdmin):
    # TODO! docstring
    list_display = (
        'tree_actions',
        'indented_title',
        'ident',
        'cfile_thumb',
        'title',
        'sites_display',
    )
    list_display_links = (
        'cfile_thumb',
        'ident',
    )
    list_editable = ('title',)
    search_fields = ('title', 'ident')
    list_filter = ('sites',)
    fieldsets = (
        (None, {
            'fields': (
                'sites',
                'icon',
                ('title', 'ident'),
                'description',
                'products',
                'keywords',
            )
        }),
        (_('Products Template'), {
            'classes': ('collapse',),
            'description': _(
                'All changes will be applied to all products '
                'included in this section'
            ),
            'fields': (
                'char_template',
                'key_features',
                'resources',
                'optional_components',
                'accessories',
                'related_products',
            ),
        }),
    )
    filter_horizontal = (
        'sites',
        'key_features',
        'resources',
        'char_template',
    )
    prepopulated_fields = {'ident': ('title',)}
    inlines = (CharToSectionInline,)
    autocomplete_fields = ('keywords',)


@admin.register(Product)
class ProductAdmin(NestedModelAdmin, TranslationAdmin):
    # TODO! docstring
    save_as = True
    list_display = (
        'cfile_thumb',
        'art',
        'title',
        'sites_display',
    )
    list_display_links = ('cfile_thumb', 'art',)
    list_editable = ('title',)
    search_fields = ('art', 'title')
    list_filter = ('sites', 'sections')
    fieldsets = (
        (None, {
            'fields': (
                'sites',
                'icon',
                ('title', 'art'),
                'description',
                'specification',
                'key_features',
                'resources',
                'keywords',
                'unit',
            )
        }),
        (_('Characteristics Template'), {
            'classes': ('collapse',),
            'description': _(
                'All characteristics applied to the this product '
                'will be automatically applied to the related sub products'
            ),
            'fields': (
                'char_template',
            ),
        }),
        (_('Related Products'), {
            'classes': ('collapse',),
            'description': _(
                'Products related by any relationship to this product'
            ),
            'fields': (
                'optional_components',
                'accessories',
                'related_products',
            ),
        }),
    )
    filter_horizontal = (
        'sites',
        'key_features',
        'resources',
        'chars',
        'char_template',
    )
    inlines = (
        BasicComponentCountInline,
        CharToProductInline,
        CarouselImageInline,
        SubProductInline,
        SectionInline,
    )
    prepopulated_fields = {'art': ('title',)}
    autocomplete_fields = ('keywords',)


@admin.register(KeyFeature)
class KeyFeatureAdmin(SortableAdminMixin, AdminImageMixin, TranslationAdmin):
    # TODO! docstring
    list_display = ('cfile_thumb', 'title', 'alt')
    list_display_links = ('cfile_thumb',)
    list_editable = ('title',)
    search_fields = ('title',)
    fields = ('cfile', ('title', 'alt'), 'description')
    readonly_fields = ('cfile_thumb',)
    prepopulated_fields = {'alt': ('title',)}


@admin.register(BasicComponent)
class BasicComponentAdmin(SortableAdminMixin, TranslationAdmin):
    # TODO! docstring
    list_display = ('name',)
    search_fields = ('name',)
    fields = ('name',)


@admin.register(Char)
class CharAdmin(SortableAdminMixin, TranslationAdmin):
    # TODO! docstring
    list_display = ('name', 'is_filter')
    list_display_links = ('name',)
    list_editable = ('is_filter',)
    list_filter = ('is_filter',)
    search_fields = ('name',)
    fields = ('name', 'is_filter', 'unit')


@admin.register(ResourceType)
class ResourceTypeAdmin(TranslationAdmin):
    # TODO! docstring
    list_display = ('name',)
    list_display_links = list_display
    search_fields = ('name',)
    fields = ('name',)
    ordering = ('name',)


@admin.register(Resource)
class ResourceAdmin(TranslationAdmin):
    # TODO! docstring
    list_display = ('cfile_thumb', 'name', 'rtype')
    list_display_links = ('cfile_thumb',)
    list_filter = ('sites', 'rtype')
    search_fields = ('name',)
    fields = ('rtype', 'name', 'cfile')
    ordering = ('name',)
    list_editable = ('name', 'rtype')
    autocomplete_fields = ('rtype',)
