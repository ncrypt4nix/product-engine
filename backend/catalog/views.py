import json
from collections import defaultdict
from django.views.generic import ListView, DetailView, TemplateView
from django.contrib.sites.models import Site
from django.shortcuts import get_object_or_404
from django.http import Http404
from django_weasyprint import WeasyTemplateResponseMixin
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from catalog.models import Section, Product
from catalog.services import ProductCartService, ProductCompareService
from catalog.serializers import ProductArtSerializer, CompareSerializer


class CatalogViewMixin:
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['menu_tree'] = Section.objects.filter(
            sites=Site.objects.get_current()
        )
        return context


class CatalogIndexView(CatalogViewMixin, ListView):
    # TODO! docstring
    model = Section
    template_name = 'catalog/index.html'

    def get_queryset(self):
        return self.model.objects.filter(
            parent=None,
            sites=Site.objects.get_current()
        )


class CatalogSectionView(CatalogViewMixin, DetailView):
    # TODO! docstring
    model = Section

    def get_template_names(self):
        if self.object.is_leaf_node():
            return ['catalog/section_final.html']
        return ['catalog/section.html']

    def get_object(self, queryset=None):
        return self.model.on_site.reverse_path_or_404(
            self.kwargs['path']
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.object.is_leaf_node():
            products = self.object.products.all()
            context['products'] = products
            filter_list = defaultdict(lambda: defaultdict(lambda: []))
            for filter in self.object.get_filters(products).order_by(
                    'char', 'value'):
                filter_list[filter.char.name][filter.value].append(
                    filter.product_id
                )
            context['filters'] = json.dumps(filter_list)
        else:
            context['children'] = self.object.children.all()
        return context


class CatalogProductView(CatalogViewMixin, DetailView):
    # TODO! docstring
    model = Product
    template_name = 'catalog/product.html'

    def get_object(self, queryset=None):
        self.section = Section.on_site.reverse_path_or_404(
            self.kwargs['section_path']
        )
        self.product = get_object_or_404(self.model, art=self.kwargs['art'])
        if self.section not in self.product.sections.all():
            raise Http404()
        return self.product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section'] = self.section
        context['carousel'] = self.product.carouselimage_set.all()
        context['order_table_headers'] = self.product.char_template.all()
        context['order_table'] = self.product.subproduct_set.all()
        context['key_features'] = self.product.get_key_features
        context['resources'] = self.product.get_resources
        context['characteristics'] = self.product.get_char_set()
        context['basic_components'] = (
            self.product.basiccomponentcount_set.all()
        )
        context['optional_components'] = self.product.get_optional_components
        context['accessories'] = self.product.get_accessories
        context['related_products'] = self.product.get_related_products
        return context


class ProductDataSheet(WeasyTemplateResponseMixin, DetailView):
    # TODO! docstring
    model = Product
    get_object = CatalogProductView.get_object

    def get_template_names(self):
        return ['catalog/product_datasheet.html']

    def get_pdf_filename(self):
        return f'{self.product.art}_datasheet.pdf'


class CompareViewSet(viewsets.ModelViewSet):
    """ViewSet to work with Compare across REST"""
    serializer_class = ProductArtSerializer
    queryset = Product.on_site.all_products()
    lookup_field = 'art'

    def list(self, request):
        service = ProductCompareService(request)
        qs = service.get_list()
        serializer = self.serializer_class(
            qs,
            many=True,
            context={'service': service},
        )
        return Response(serializer.data)

    def create(self, request):
        art = request.POST.get('art')
        count = request.POST.get('count', 1)
        if not art:
            return Response(False)
        ProductCompareService(request).add_product(art, count)
        return Response(True)

    def update(self, request, art):
        count = request.POST.get('count', 1)
        ProductCompareService(request).add_product(art, count)
        return Response(True)

    def destroy(self, request, art):
        ProductCompareService(request).remove_product(art)
        return Response(True)

    @action(methods=['post'], detail=False)
    def clear(self, request):
        ProductCompareService(request).clear()
        return Response(True)

    @action(methods=['get'], detail=False)
    def sections(self, request):
        return Response(
            [i.title for i in ProductCompareService(request).get_sections()]
        )

    @action(methods=['get'], detail=False)
    def section_view(self, request):
        """get product with chars in section"""
        service = ProductCompareService(request)
        qs = service.get_list()
        serializer = CompareSerializer(
            qs,
            many=True,
            context={'service': service},
        )
        return Response(serializer.data)


class CartViewSet(viewsets.ModelViewSet):
    """ViewSet to work with Cart across REST"""
    serializer_class = ProductArtSerializer
    queryset = Product.on_site.all_products()
    lookup_field = 'art'

    def list(self, request):
        service = ProductCartService(request)
        qs = service.get_list()
        serializer = ProductArtSerializer(
            qs,
            many=True,
            context={'service': service},
        )
        return Response(serializer.data)

    def create(self, request):
        art = request.POST.get('art')
        count = request.POST.get('count', 1)
        if not art:
            return Response(False)
        ProductCartService(request).add_product(art, count)
        return Response(True)

    def update(self, request, art):
        count = request.POST.get('count', 1)
        ProductCartService(request).add_product(art, count)
        return Response(True)

    def destroy(self, request, art):
        ProductCartService(request).remove_product(art)
        return Response(True)

    @action(methods=['post'], detail=False)
    def clear(self, request):
        ProductCartService(request).clear()
        return Response(True)


class CompareInSectionView(CatalogViewMixin, TemplateView):
    template_name = 'catalog/compare.html'
