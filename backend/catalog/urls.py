from rest_framework import routers
from django.urls import path
from catalog.views import (
    CatalogIndexView,
    CatalogSectionView,
    CatalogProductView,
    ProductDataSheet,
    CompareViewSet,
    CartViewSet,
    CompareInSectionView,
)


router = routers.SimpleRouter()

router.register(r'api/compare', CompareViewSet, basename='compare')
router.register(r'api/cart', CartViewSet, basename='cart')


urlpatterns = router.urls + [
    path(
        'compare/',
        CompareInSectionView.as_view(),
        name='compare'
    ),
    path(
        'compare/<slug:section>/',
        CompareInSectionView.as_view(),
        name='compare-section'
    ),
    path(
        'section/<path:section_path>/product/<slug:art>/datasheet/',
        ProductDataSheet.as_view(),
        name='catalog_product_datasheet'
    ),
    path(
        'section/<path:section_path>/product/<slug:art>/',
        CatalogProductView.as_view(),
        name='catalog_product'
    ),
    path(
        'section/<path:path>/',
        CatalogSectionView.as_view(),
        name="catalog_section"
    ),
    path('', CatalogIndexView.as_view(), name='catalog-index'),
]
