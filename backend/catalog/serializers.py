from rest_framework import serializers
from catalog.models import Product


class ProductArtSerializer(serializers.ModelSerializer):
    """ Serializer for compare and cart pages
        needed extra context service = ProductSessionList based
    """
    count = serializers.SerializerMethodField()
    icon = serializers.URLField(source='real_product.icon.url', read_only=True)
    description = serializers.CharField(
        source='real_product.description',
        read_only=True
    )
    unit = serializers.CharField(
        source='real_product.get_unit_display',
        read_only=True
    )

    class Meta:
        model = Product
        fields = ('icon', 'art', 'description', 'count',  'unit')

    def get_count(self, obj):
        return self.context['service'].get_count(obj.art)


class CompareSerializer(serializers.ModelSerializer):
    """Serializer products with characteristics"""
    icon = serializers.URLField(source='real_product.icon.url', read_only=True)
    description = serializers.CharField(
        source='real_product.description',
        read_only=True
    )
    chars = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = ('icon', 'art', 'description', 'chars')

    def get_chars(self, product):
        return {
            char.char.name: char.value
            for char in product.real_product.get_char_set()
        }
