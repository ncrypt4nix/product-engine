import abc
from collections import defaultdict
from django.contrib.sessions.backends.base import UpdateError
from catalog.models import Product, Section


class ProductSessionList(abc.ABC):
    """Abstract class to work with product in session"""
    SESSION_KEY = property(abc.abstractmethod(lambda _: _))

    def __init__(self, request):
        """init session if key does not exist"""
        self.session = request.session
        if not self.session.get(self.SESSION_KEY):
            self.session[self.SESSION_KEY] = defaultdict(lambda: 0)

    def __del__(self):
        """Save session before close work"""
        try:
            self.session.save()
        except UpdateError:  # excepting to tests
            pass

    def add_product(self, art: str, count: int):
        """Add product to product session list
        Exceptions: ObjectDoesNotExist
        """
        # just check to product exist
        Product.on_site.get_product_from_art(
            art=art
        )
        self.session[self.SESSION_KEY][art] = count

    def remove_product(self, art: str):
        """Remove product from product session list"""
        self.session[self.SESSION_KEY].pop(art)

    def get_list(self):
        """Get products union queryset"""
        return Product.on_site.filter_product_from_art_list(
            self.session[self.SESSION_KEY].keys()
        )

    def get_count(self, art: str):
        """Get count value from session to art"""
        return self.session[self.SESSION_KEY].get(art)

    def clear(self):
        """Clear product session list"""
        self.session[self.SESSION_KEY] = defaultdict(lambda: 0)

    def get_sections(self):
        """Get Sections including product session list"""
        # TODO slice duplicates and not needed sections
        return Section.objects.filter(
            products__in=[i.real_product_pk for i in self.get_list()]
        )


class ProductCompareService(ProductSessionList):
    """Service to work with product in compare"""
    SESSION_KEY = 'compare_product_list'


class ProductCartService(ProductSessionList):
    """Service to work with product in compare"""
    SESSION_KEY = 'cart_product_list'
