from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey
from sortedm2m.fields import SortedManyToManyField
from engine.core.interfaces import ContentImageInterface, ContentFileInterface
from catalog.interfaces import (
    CatalogInterface,
    ProductCharInterface,
    CharInterface,
)
from catalog.metaclasses import SectionMeta
from catalog.mixins import ParentCharMixin, ChildCharMixin
from catalog.managers import SectionManager
from engine.core.mixins import OnSiteMixin
from engine.core.decorators import cached_method
from engine.core.fields import OrderField
from engine.core.utils import Unit


class Section(
    MPTTModel,
    CatalogInterface,
    CharInterface,
    ParentCharMixin,
    metaclass=SectionMeta
):
    # TODO! docstring
    class Meta:
        verbose_name = _('Section')
        verbose_name_plural = _('Sections')
        indexes = [
            models.Index(fields=['ident']),
        ]

    ident = models.SlugField(
        _('Identifier'),
        unique=True,
        max_length=64,
        db_index=True,
        help_text=_('Used to generate url address'),
    )
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name='children',
        verbose_name=_('Parent section'),
        help_text=_('Nest the current section in the parent'),
    )
    products = SortedManyToManyField(
        to='catalog.Product',
        verbose_name=_('Products'),
        related_name='sections',
        blank=True,
        help_text=_('List of products in this section'),
    )
    on_site = SectionManager()

    def __str__(self):
        return self.title

    @cached_method
    def get_url_path(self):
        return ''.join([
            f'{i.ident}' + ('/' if i != self else '')
            for i in self.get_ancestors(include_self=True)
        ])

    @cached_method
    def get_absolute_url(self):
        return reverse(
            'catalog:catalog_section',
            kwargs={'path': self.get_url_path()}
        )


class Product(
    ProductCharInterface,
    CatalogInterface,
    ParentCharMixin,
    ChildCharMixin
):
    # TODO! docstring
    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    class CarouselImage(ContentImageInterface):
        # TODO! docstring
        class Meta:
            verbose_name = _('Carousel Image')
            verbose_name_plural = _('Carousel Images')
            ordering = ('order_index',)

        order_index = OrderField()
        product = models.ForeignKey(
            'catalog.Product',
            on_delete=models.CASCADE,
            verbose_name=_('Product'),
            help_text=_('Picture-related product')
        )
        caption = models.CharField(
            _('Text for picture'),
            max_length=32,
            blank=True,
            help_text=_('Text is displayed below the picture in the carousel')
        )

    class SubProduct(ProductCharInterface, ChildCharMixin):
        # TODO! docstring
        class Meta:
            verbose_name = _('Sub Product')
            verbose_name_plural = _('Sub Products')
            ordering = ('order_index',)

        order_index = OrderField()
        product = models.ForeignKey(
            'catalog.Product',
            on_delete=models.CASCADE,
            verbose_name=_('Product'),
            help_text=_('Common product for sub product')
        )

        def save(self, *args, **kwargs):
            super().save(*args, **kwargs)
            self.sites.set(self.product.sites.all())

        @property
        def char_parent(self):
            return self.product._meta.model.objects.filter(
                pk=self.product.pk
            )

        @property
        def real_product_pk(self):
            return self.product_id

        @property
        def real_product(self):
            return self.product

    specification = models.TextField(
        _('Specification'),
        help_text=_('Detailed product description'),
    )
    basic_components = models.ManyToManyField(
        to='catalog.BasicComponent',
        verbose_name=_('Basic Components'),
        help_text=_('Basic equipment for the current product'),
        blank=True,
        through='catalog.BasicComponentCount',
        through_fields=('product', 'component'),
    )
    # TODO! unit with choices
    unit = models.PositiveSmallIntegerField(
        _('Unit'),
        choices=Unit.choices,
        default=Unit.pc,
        help_text=_('Unit of Characteristic'),
    )

    @property
    def real_product_pk(self):
        return self.pk

    @property
    def real_product(self):
        return self

    @property
    def catalog_parent(self):
        return self.sections.all()

    @property
    def char_parent(self):
        return self.sections.all()

    @cached_method
    def get_absolute_url(self):
        return reverse(
            'catalog:catalog_product',
            kwargs={
                'section_path': self.sections.first().get_url_path(),
                'art': self.art,
            }
        )


class KeyFeature(ContentImageInterface):
    # TODO! docstring
    class Meta:
        verbose_name = _('Key Feature')
        verbose_name_plural = _('Key Features')
        ordering = ('order_index',)

    order_index = OrderField()
    title = models.CharField(
        _('Header'),
        max_length=64,
        help_text=_('Header for key feature'),
    )
    description = models.TextField(
        _('Description'),
        help_text=_('Description of the key product features'),
    )

    def __str__(self):
        return self.title


class BasicComponent(models.Model):
    # TODO! docstring
    class Meta:
        verbose_name = _('Basic Component')
        verbose_name_plural = _('Basic Components')
        ordering = ('order_index',)

    order_index = OrderField()
    name = models.CharField(
        _('Name'),
        max_length=64,
        unique=True,
        help_text=_('Name of a product component')
    )

    def __str__(self):
        return self.name


class BasicComponentCount(models.Model):
    # TODO! docstring
    class Meta:
        verbose_name = _('Basic Component Count')
        verbose_name_plural = _('Basic Components Count')
        unique_together = ('product', 'component')

    product = models.ForeignKey(
        'catalog.Product',
        on_delete=models.CASCADE,
    )
    component = models.ForeignKey(
        'catalog.BasicComponent',
        on_delete=models.CASCADE,
    )
    count = models.PositiveSmallIntegerField(
        _('Count'),
        default=1,
        help_text=_('Count component'),
    )


class Char(models.Model):
    # TODO! docstring
    class Meta:
        verbose_name = _('Characteristic')
        verbose_name_plural = _('Characteristics')
        ordering = ('order_index',)

    order_index = OrderField()
    name = models.CharField(
        _('Name'),
        max_length=64,
        unique=True,
        help_text=_('Name of characteristic')
    )

    unit = models.PositiveSmallIntegerField(
        _('Unit'),
        choices=Unit.choices,
        null=True,
        blank=True,
        help_text=_('Unit of Characteristic'),
    )

    # flags
    is_filter = models.BooleanField(
        _('Is Filter'),
        default=False,
        help_text=_(
            'if checked, the characteristic '
            'will be used as a filter in the catalog'
        )
    )

    def __str__(self):
        return f'{self.name} {self.unit}'


class ResourceType(models.Model):
    # TODO! docstring
    class Meta:
        verbose_name = _('Resource Type')
        verbose_name_plural = _('Resource Types')

    name = models.CharField(
        _('Name'),
        max_length=64,
        unique=True,
        help_text=_('Display resource type name')
    )

    def __str__(self):
        return self.name


class Resource(ContentFileInterface, OnSiteMixin):
    # TODO! docstring
    class Meta:
        verbose_name = _('Resource')
        verbose_name_plural = _('Resource')

    rtype = models.ForeignKey(
        'catalog.ResourceType',
        verbose_name=_('Type'),
        on_delete=models.CASCADE,
        help_text=_('Type of resource for grouping it with other resources'),
    )
    name = models.CharField(
        _('Name'),
        max_length=64,
        unique=True,
        help_text=_('Display file name')
    )

    def __str__(self):
        return self.name
