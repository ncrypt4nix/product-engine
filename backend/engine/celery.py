from __future__ import absolute_import, unicode_literals
import os
from celery import Celery

BROKER_URL = 'redis://redis-product:6379/0'
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'engine.settings')
app = Celery('engine', broker=BROKER_URL)
app.config_from_object('django.conf:settings', namespace='CELERY')

app.conf.broker_url = BROKER_URL
app.conf.enable_utc = True

app.autodiscover_tasks()
