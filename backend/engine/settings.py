import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SRC_DIR = os.path.dirname(BASE_DIR)
PROJECT_DIR = os.path.dirname(SRC_DIR)

SITE_ID = os.getenv('SITE_ID', 1)
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.getenv('DEBUG')

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('SECRET_KEY')

ALLOWED_HOSTS = [os.getenv('ALLOWED_HOST')]

# Application definition
INSTALLED_APPS = [
    'modeltranslation',
] + [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admindocs',
    'django.contrib.sites',
] + [
    'rest_framework',
    'nested_admin',
    'logentry_admin',
    'sorl.thumbnail',
    'mptt',
    'sortedm2m',
    'adminsortable2',
    'sitetree',
    'widget_tweaks',
    'django_bootstrap_breadcrumbs',
    'drf_yasg',
    'django_celery_results',
    'django_celery_beat',
] + [
    'engine.core',
    'catalog',
    'api',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.admindocs.middleware.XViewMiddleware',
]

ROOT_URLCONF = os.getenv('ROOT_URLCONF', 'engine.urls')

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(SRC_DIR, 'frontend', 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'engine.core.context_processors.core_template_init',
            ],
        },
    },
]

WSGI_APPLICATION = 'engine.wsgi.application'


# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('DEFAULT_DATABASE_NAME'),
        'USER': os.getenv('DEFAULT_DATABASE_USER'),
        'PASSWORD': os.getenv('DEFAULT_DATABASE_PASSWORD'),
        'HOST': 'db-product',
        'PORT': 5432,
    }
}


# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Cache
CACHE_STORAGE_TIME = 15 * 60
CACHE_TEMPLATE_TIME = 15 * 60
CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': 'redis://redis-product:6379/1',
        'OPTIONS': {
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'SOCKET_TIMEOUT': 15,
            'SOCKET_CONNECT_TIMEOUT': 15,
            'COMPRESSOR_CLASS': 'redis_cache.compressors.ZLibCompressor',
            'COMPRESSOR_CLASS_KWARGS': {
                # 0 - 9
                # 0 - no compression;
                # 1 - fastest, biggest;
                # 9 - slowest, smallest
                'level': 4,  
            },
        }
    },
}


# Celery
CELERY_RESULT_BACKEND = 'django-db'


# Internationalization
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Model Translation
gettext = lambda s: s
LANGUAGES = (
    ('ru', gettext('Russian')),
    ('en', gettext('English')),
)
MODELTRANSLATION_DEFAULT_LANGUAGE = 'ru'
MODELTRANSLATION_DEBUG = DEBUG


# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PROJECT_DIR, 'static')
STATICFILES_DIRS = (os.path.join(SRC_DIR, 'frontend', 'static'),)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media')

NO_IMAGE_PATH = os.path.join(MEDIA_ROOT, 'no_image.png')
FAVICON_PATH = os.path.join(MEDIA_ROOT, 'favicon.ico')

# Django Rest Framework
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': [
        'rest_framework.renderers.JSONRenderer',
    ]
}

if DEBUG:
    REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'].append(
        'rest_framework.renderers.BrowsableAPIRenderer'
    )
