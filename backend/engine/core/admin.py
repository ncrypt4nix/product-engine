from django.contrib import admin
from django.contrib.auth import get_permission_codename
from django.shortcuts import redirect
from engine.core.models import (
    Setting,
    KeyWord
)


@admin.register(Setting)
class SettingsAdmin(admin.ModelAdmin):
    # TODO! docstring
    list_display = ('ident', 'site')
    fields = ('site', ('ident', 'description'), ('value', 'attachment'))
    list_filter = ('site', 'ident')
    radio_fields = {'site': admin.VERTICAL}
    actions = ['update_favicon', 'update_noimage']

    def update_favicon(self, request, queryset):
        return redirect('core:update_favicon')
    update_favicon.allowed_permissions = ('can_update_favicon',)

    def has_can_update_favicon_permission(self, request):
        opts = self.opts
        codename = get_permission_codename('can_update_favicon', opts)
        return request.user.has_perm('%s.%s' % (opts.app_label, codename))

    def update_noimage(self, request, queryset):
        return redirect('core:update_noimage')
    update_noimage.allowed_permissions = ('can_update_image_noimage',)

    def has_can_update_image_noimage_permission(self, request):
        opts = self.opts
        codename = get_permission_codename('can_update_image_noimage', opts)
        return request.user.has_perm('%s.%s' % (opts.app_label, codename))


@admin.register(KeyWord)
class KeyWordAdmin(admin.ModelAdmin):
    # TODO! docstring
    list_display = ('word',)
    fields = ('word',)
    search_fields = ('word',)
