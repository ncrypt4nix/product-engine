from django.db import models
from django.utils.html import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from sorl.thumbnail import ImageField, get_thumbnail


class SEOObjectMixin(models.Model):
    # TODO! docstring
    class Meta:
        abstract = True

    title = models.CharField(
        _('Title'),
        max_length=64,
        help_text=_('Used in title tag and SEO')
    )
    description = models.TextField(
        _('Description'),
        help_text=_('Short description Used in SEO')
    )
    icon = ImageField(
        _('Icon'),
        upload_to='seo/icons',
        max_length=128,
        help_text=_('Used in SEO and as picture')
    )
    created = models.DateTimeField(
        _('Created'),
        auto_now_add=True,
        help_text=_('Content posting date'),
    )
    updated = models.DateTimeField(
        _('Updated'),
        auto_now=True,
        help_text=_('Last modified content date'),
    )
    keywords = models.ManyToManyField(
        to='core.KeyWord',
        verbose_name=_('Keywords'),
        help_text=_('Required for search engines'),
    )

    @mark_safe
    def cfile_thumb(self):
        # TODO! docstring
        return '<img src="{src}" alt="{alt}">'.format(
            src=get_thumbnail(
                self.icon,
                '32x32',
                crop='center',
                quality=30
            ).url,
            alt=self.icon.name
        )


class OnSiteMixin(models.Model):
    # TODO! docstring
    class Meta:
        abstract = True

    # To fix Django error with disappearing records in the admin panel
    # Caution! This creates empty migrations.
    objects = models.Manager()
    on_site = CurrentSiteManager()
    sites = models.ManyToManyField(
        Site,
        blank=True,
        verbose_name=_('Sites'),
        help_text=_('The site on which this content should be placed'),
    )

    @property
    def sites_display(self):
        # TODO! docstring
        return ''.join([i.name for i in self.sites.all()])
