from django.db import models
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _
from engine.core.fields import PreviewFileField
from engine.core.managers import SettingManager


class Setting(models.Model):
    # TODO! docstring
    class Meta:
        verbose_name = _('Setting')
        verbose_name_plural = _('Settings')
        unique_together = ('site', 'ident')
        permissions = (
            ('can_update_favicon', 'Can update favicon'),
            ('can_update_image_noimage', 'Can update image "noimage"')
        )

    on_site = SettingManager()
    site = models.ForeignKey(
        Site,
        on_delete=models.CASCADE,
        verbose_name=_('Site'),
        help_text=_('The site for which the setting will work'),
    )
    ident = models.SlugField(
        verbose_name=_('Identifier'),
        max_length=64,
        help_text=_('Unique identifier of the settings option'),
        db_index=True,
    )
    value = models.CharField(
        verbose_name=_('Value'),
        max_length=128,
        help_text=_('Setting option value'),
    )
    description = models.CharField(
        verbose_name=_('Description'),
        max_length=128,
        help_text=_('Short description of the settings option'),
    )
    attachment = PreviewFileField(
        _('Attachment'),
        null=True,
        blank=True,
        help_text=_('Some settings require a file, such as a brand logo'),
        upload_to='settings/',
    )

    def __str__(self):
        return f'{self.site}: {self.ident}'


class KeyWord(models.Model):
    # TODO! docstring
    word = models.CharField(
        _('Key Word'),
        unique=True,
        max_length=16,
        help_text=_('Required for search engines')
    )

    def __str__(self):
        return self.word
