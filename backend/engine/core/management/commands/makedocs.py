from django.core.management.base import BaseCommand
from django.conf import settings
import os


class Command(BaseCommand):

    def search_apps(self, apps_dir, directory):
        """
        Recurse func, for search all apps  in all dirs
        :param apps_dir: on start-empty list for stacking founds apps
        :param directory: current dir for checking
        :return apps_dir:
        """
        def is_app(check_dir=directory):
            """
            For check whether the directory is an application
            :param check_dir:
            :return bool:
            """
            return os.path.exists(
                os.path.join(check_dir, 'apps.py')
            )
        if is_app():
            apps_dir.append(directory)
        dirs = [
            f for f in os.listdir(directory)
            if os.path.isdir(os.path.join(directory, f))
        ]
        for d in dirs:
            cur_dir = os.path.join(directory, d)
            if is_app(cur_dir):
                apps_dir.append(cur_dir)
            else:
                self.search_apps(apps_dir, cur_dir)
        return apps_dir

    def handle(self, *args, **options):
        tabs = '      '
        filename = 'index.rst'
        base_dir = settings.BASE_DIR
        docs_dir = os.path.join(
            os.path.dirname(base_dir),
            'docs',
            os.path.basename(base_dir)
        )

        def title(insert_str, symbol='=', prefix=''):
            """
            Creating a text block for the file
            header and subheader inside the page
            like this:
                **********************
                Module engine.core
                **********************
            or this:
                ==============
                Decorators
                ==============
            :param insert_str:
            :param symbol:
            :param prefix:
            :return list: text block
            """
            return [
                '',
                symbol * (len(insert_str + prefix) + 4),
                prefix + insert_str,
                symbol * (len(insert_str + prefix) + 4),
            ]

        def text_block(insert_str):
            """
            Creating text block for init modules in file apps
            like this:
                .. automodule:: engine.core.context_processors
                    :members:
                    :undoc-members:
            :param insert_str:
            :return list: text block
            """
            return [
                '.. automodule:: ' + insert_str,
                tabs + ':members:',
                tabs + ':undoc-members:'
            ]

        def text_block_main(main_app_dir_in_docs):
            """
            Creating string, for insert in main index.rst
            like this:
                backend/engine/core/index
            :param main_app_dir_in_docs:
            :return str:
            """
            return tabs + os.path.join(
                main_app_dir_in_docs,
                'index'
            ) + '\n\n'

        apps_dir = self.search_apps([], base_dir)
        path_main_index = os.path.join(os.path.dirname(docs_dir), filename)
        for app_num, app_dir in enumerate(apps_dir):
            index_app = []
            # search all files(modules) in app
            files_name = [
                os.path.splitext(item)[0] for item in os.listdir(app_dir)
                if os.path.isfile(
                    os.path.join(app_dir, item)
                ) and '__' not in item
            ]
            doc_app_dir = app_dir.replace(base_dir, docs_dir)
            if not os.path.exists(doc_app_dir):
                os.makedirs(doc_app_dir)
            app_dir_in_doc = app_dir.replace(base_dir + '/', '')
            app_name = app_dir_in_doc.replace('/', '.')
            # adding a page title, ...
            index_app.extend(title(app_name, '*', 'Module '))
            for name in files_name:
                # ... subheading modules, ...
                index_app.extend(title(name.capitalize()))
                # ... init modules, ...
                index_app.extend(text_block(app_name + '.' + name))
            with open(os.path.join(doc_app_dir, filename), 'w') as f:
                for string in index_app:
                    # ... and writing everything to a file
                    f.write(string+'\n')

            # read main index.rst ...
            with open(path_main_index, 'r') as f:
                writen_str = text_block_main(
                    os.path.join(
                        os.path.basename(base_dir),
                        app_dir_in_doc
                    )
                )
                main_file = f.readlines()
                strip_main_file = [item.strip() for item in main_file]
                # ...for search duplicate ...
                if writen_str.strip() in strip_main_file:
                    continue
                # ...search appropriate place if not duplicate...
                write_here = strip_main_file.index(
                    ':caption: Contents:'
                ) + 2 + app_num
                # ...and to insert init string in the appropriate place...
                main_file.insert(
                    write_here,
                    writen_str
                )
            # and write all this
            with open(path_main_index, 'w') as f:
                f.writelines(main_file)

        # call command 'make html'
        os.chdir(os.path.join(os.path.dirname(base_dir), 'docs'))
        command = os.system('make html')
        if not command:
            self.stdout.write(
                self.style.SUCCESS(
                    'Collect docs done!\n'
                    'For seeing, open docs/_build/html/index.html'
                )
            )
        else:
            self.stdout.write(
                self.style.ERROR(
                    'command error:{}'.format(command)
                )
            )
