from functools import wraps
from django.conf import settings
from django.core.cache import cache


def cached_method(func):
    # TODO! docstring
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        hash_key = 'hashed_method_{func_hash}_{model_hash}_{self_hash}'.format(
            func_hash=func.__hash__(),
            model_hash=self._meta.__hash__(),
            self_hash=self.__hash__()
        )
        if cache.get(hash_key):
            return cache.get(hash_key)
        value = func(self, *args, **kwargs)
        cache.add(hash_key, value, settings.CACHE_STORAGE_TIME)
        return value
    return wrapper
