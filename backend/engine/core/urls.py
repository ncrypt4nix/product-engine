from django.urls import path
from engine.core.views import (
    UpdateFaviconFormView,
    UpdateNoImageFormView
)

urlpatterns = [
    path(
        'admin/core/setting/update_favicon/',
        UpdateFaviconFormView.as_view(),
        name='update_favicon'
    ),
    path(
        'admin/core/setting/update_noimage/',
        UpdateNoImageFormView.as_view(),
        name='update_noimage'
    )
]
