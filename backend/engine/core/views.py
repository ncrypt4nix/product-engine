import os
from django.conf import settings
from django.views.generic.edit import FormView
from engine.core.forms import (
    UpdateFaviconForm,
    UpdateNoImageForm
)


def upload_file(file, path):
    file.name = os.path.basename(path)
    with open(path, 'wb') as handle:
        handle.write(file.read())


class UpdateFaviconFormView(FormView):
    form_class = UpdateFaviconForm
    template_name = 'engine/core/admin_update_image.html'
    success_url = '/admin/core/setting/'

    def form_valid(self, form):
        upload_file(form.cleaned_data['file'], settings.FAVICON_PATH)
        return super().form_valid(form)


class UpdateNoImageFormView(FormView):
    form_class = UpdateNoImageForm
    template_name = 'engine/core/admin_update_image.html'
    success_url = '/admin/core/setting/'

    def form_valid(self, form):
        upload_file(form.cleaned_data['file'], settings.NO_IMAGE_PATH)
        return super().form_valid(form)
