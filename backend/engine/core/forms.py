from django import forms
from django.utils.translation import gettext as _
from engine.core.validators import validate_favicon, validate_noimage


class UpdateNoImageForm(forms.Form):
    file = forms.FileField(
        validators=[validate_noimage],
        label=_('Select the .png image to update NoImage:')
    )


class UpdateFaviconForm(forms.Form):
    file = forms.FileField(
        validators=[validate_favicon],
        label=_('Select the .ico image to update favicon:')
    )
