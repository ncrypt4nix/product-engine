import tempfile
from PIL import Image
from django.utils import translation
from django.test import Client, override_settings, SimpleTestCase


class TestBaseMeta(type):
    # TODO! docstring
    def __new__(cls, name, bases, dct):
        # TODO! docstring
        _class = super().__new__(cls, name, bases, dct)
        if not issubclass(_class, SimpleTestCase):
            return _class
        return override_settings(MEDIA_ROOT=tempfile.gettempdir())(_class)


class TestBaseMixin(metaclass=TestBaseMeta):
    files = []

    def get_temp_image(self):
        tmp_file = tempfile.NamedTemporaryFile(suffix='.jpg')
        # to keep files into RAM on test time
        self.files.append(tmp_file)
        size = (200, 200)
        color = (255, 0, 0)
        image = Image.new('RGB', size, color)
        image.save(tmp_file)
        return tmp_file

    def setUp(self):
        # set default test language
        super().setUp()
        translation.activate('en')
        # create client object
        self.client = Client()
