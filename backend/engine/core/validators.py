from django.utils.translation import gettext as _
from django.core.exceptions import ValidationError


def validate_favicon(value):
    valid_extensions = ['image/x-icon']
    if value.content_type not in valid_extensions:
        raise ValidationError(_('Unsupported file extension.'))


def validate_noimage(value):
    valid_extensions = ['image/png']
    if value.content_type not in valid_extensions:
        raise ValidationError(_('Unsupported file extension.'))
