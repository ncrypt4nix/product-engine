from django.contrib.sites.models import Site
from django.conf import settings
from engine.core.models import Setting


def core_template_init(request):
    # TODO! docstring
    return {
        'site_logo': Setting.on_site.get_site_logo(),
        'site': Site.objects.get_current(),
        'cache_timeout': settings.CACHE_TEMPLATE_TIME,
        'google_analytics_code': Setting.on_site.get_google_analytics_code(),
        'yandex_metrika_code': Setting.on_site.get_yandex_metrika_code(),
    }
