import os
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.db.models.fields.files import FieldFile
from preview_generator.manager import PreviewManager
from preview_generator.exception import UnavailablePreviewType


class PreviewFieldFile(FieldFile):
    # TODO! docstring

    @property
    def preview(self) -> str:
        # TODO! docstring
        cache_path = os.path.join(os.path.dirname(self.path), 'preview')
        manager = PreviewManager(cache_path, create_folder=True)
        try:
            return manager.get_jpeg_preview(self.path, width=1024, height=1024)
        except UnavailablePreviewType:
            return settings.NO_IMAGE_PATH

    def delete(self, *args, **kwargs) -> None:
        # TODO! docstring
        if not self:
            return
        self.storage.delete(self.preview)
        super().delete(*args, **kwargs)


class PreviewFileField(models.FileField):
    # TODO! docstring
    attr_class = PreviewFieldFile


class OrderField(models.PositiveIntegerField):
    # TODO! docstring
    def __init__(self, *args, **kwargs):
        kwargs['verbose_name'] = _('Order')
        kwargs['default'] = 0
        kwargs['help_text'] = _(
            'Set the desired value for the sort order of the object'
        )
        super().__init__(*args, **kwargs)
