from django.contrib.sites.managers import CurrentSiteManager
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _


class SettingManager(CurrentSiteManager):
    def get_site_logo(self):
        logo, created = super().get_queryset().get_or_create(
            ident='site_logo',
            defaults={
                'description': _('Upload image with site logo'),
                'value': _('Logo'),
                'site': Site.objects.get_current(),
            }
        )
        return logo

    def get_google_analytics_code(self):
        code, create = super().get_queryset().get_or_create(
            ident='google_analytics_code',
            defaults={
                'description': _(
                    'Add "Google Analytics" code ' +
                    'to track statistics in "Google Analytics"' +
                    'in this site'
                ),
                'value': _('CODE'),
                'site': Site.objects.get_current(),
            }
        )
        return code

    def get_yandex_metrika_code(self):
        code, create = super().get_queryset().get_or_create(
            ident='yandex_metrika_code',
            defaults={
                'description': _(
                    'Add "Yandex Metrika" code ' +
                    'to track statistics in "Yandex Metrika"' +
                    'in this site'
                ),
                'value': _('CODE'),
                'site': Site.objects.get_current(),
            }
        )
        return code
