import abc
import datetime
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.html import mark_safe
from sorl.thumbnail import ImageField, get_thumbnail
from engine.core.mixins import OnSiteMixin, SEOObjectMixin
from engine.core.metaclasses import AbstractModelMeta
from engine.core.fields import PreviewFileField


class ContentInterface(
    SEOObjectMixin,
    OnSiteMixin
):
    # TODO! docstring
    class Meta:
        abstract = True


class ContentFileBaseAbstract(models.Model, metaclass=AbstractModelMeta):
    # TODO! docstring
    class Meta:
        abstract = True

    cfile = property(abc.abstractmethod(lambda _: _))

    @staticmethod
    def path_save_generate(instance, filename: str) -> str:
        # TODO! docstring
        return '{model_name}/{time_hash}/{filename}'.format(
            model_name=instance._meta.model_name,
            time_hash=datetime.datetime.now().__hash__(),
            filename=filename
        )

    @abc.abstractmethod
    def cfile_thumb(self):
        # TODO! docstring
        pass
    cfile_thumb.allow_tags = True


class ContentImageInterface(
    ContentFileBaseAbstract
):
    # TODO! docstring
    class Meta:
        abstract = True

    cfile = ImageField(
        _('Image'),
        upload_to=ContentFileBaseAbstract.path_save_generate,
        max_length=128,
        help_text=_('Used in SEO and as picture')
    )
    alt = models.CharField(
        _('Alt Attribute'),
        max_length=16,
        help_text=_('Specifies an alternate text for an image')
    )

    @mark_safe
    def cfile_thumb(self):
        # TODO! docstring
        return '<img src="{src}" alt="{alt}">'.format(
            src=get_thumbnail(
                self.cfile,
                '32x32',
                crop='center',
                quality=30
            ).url,
            alt=self.alt
        )


class ContentFileInterface(
    ContentFileBaseAbstract
):
    # TODO! docstring
    class Meta:
        abstract = True

    cfile = PreviewFileField(
        _('File'),
        upload_to=ContentFileBaseAbstract.path_save_generate,
        max_length=128,
        help_text=_('Used in SEO and as picture')
    )

    @mark_safe
    def cfile_thumb(self):
        # TODO! docstring
        return '<img src="{src}" alt="{alt}">'.format(
            src=get_thumbnail(
                self.cfile.preview,
                '32x32',
                crop='center',
                quality=30
            ).url,
            alt=self.name
        )
