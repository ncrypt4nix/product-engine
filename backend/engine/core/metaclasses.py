import abc
from django.db.models.base import ModelBase
from mptt.models import MPTTModelBase


class AbstractModelMeta(abc.ABCMeta, ModelBase):
    # TODO! docstring
    pass


class DjangoMPTTMeta(MPTTModelBase, AbstractModelMeta):
    # TODO! docstring
    pass
