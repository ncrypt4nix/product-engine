from django.contrib import admin
from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns
from django.views.i18n import JavaScriptCatalog
from sitetree.sitetreeapp import register_i18n_trees


urlpatterns = i18n_patterns(
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    path('_nested_admin/', include('nested_admin.urls')),
    path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),
    path(
        'catalog/',
        include(('catalog.urls', 'catalog'), namespace='catalog')
    ),
    path(
        'core/',
        include(('engine.core.urls', 'engine.core'), namespace='core')
    ),
    path(
        'api/',
        include(('api.urls', 'api'), namespace='api')
    ),

)

register_i18n_trees(['main_menu'])
