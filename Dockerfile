FROM python:3.8-buster

ENV PYTHONUNBUFFERED 1
EXPOSE 8000

RUN apt -y update && \
    apt -y upgrade && \
    apt -y install --no-install-recommends \
        gettext \
        ruby-full \
        nodejs \
        npm \
        zlib1g-dev \
        libjpeg-dev \
        python3-pythonmagick \
        inkscape \
        xvfb \
        poppler-utils \
        libfile-mimeinfo-perl \
        qpdf \
        libimage-exiftool-perl \
        ufraw-batch \
        ffmpeg \
        libreoffice \
        && \
    apt -y autoclean && \
    apt -y autoremove && \
    gem install sass

RUN groupadd wsgi && \
    useradd -m -g wsgi wsgi

RUN mkdir /srv/project \
          /srv/media \
          /srv/static \
          /srv/logs && \
    chown -R wsgi:wsgi /srv/*
WORKDIR /srv/project
VOLUME /srv/media \
       /srv/static \
       /srv/logs

COPY backend/requirements.txt /srv/project/
RUN pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt && \
    rm /srv/project/requirements.txt
USER wsgi

COPY --chown=wsgi:wsgi . /srv/project/
CMD bash /srv/project/docker/project/run_wsgi.sh
