# product-engine

Engine for product sites Tayle company. Use as a microservice to your project.

Before up the project you need to create a file `.env` with the following contents:
```
SECRET_KEY=****
ALLOWED_HOST=localhost
DEFAULT_DATABASE_NAME=db-name
DEFAULT_DATABASE_USER=db-user
DEFAULT_DATABASE_PASSWORD=db-password
DEBUG=False
DEPLOY=prod
SITE_ID=1
ROOT_URLCONF=engine.local_urls
```
And if you need local settings for development, add them to this file
```
DJANGO_SETTINGS_MODULE=engine.settings_local
```
Then in the file docker-compose.local.yml in environment settings add this variable